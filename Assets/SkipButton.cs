﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(Restart.Skip);
	}
}
