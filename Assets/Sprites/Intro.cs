﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour {

    [SerializeField]
    private GameObject vnEvent;
    private bool complete;

	// Update is called once per frame
	void Update () {
        if (vnEvent || complete)
            return;
        LoadingManager.Instance.LoadScene(Scenes.Game);
        complete = true;
	}
}
