﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FadeEffect : MonoBehaviour {

    private Text text;
    [SerializeField]
    private float fadeSpeed = -0.05f;
    private Color temp;

	void Start () {
        text = GetComponent<Text>();
	}
	
	void FixedUpdate () {
        temp = text.color;
        temp.a += fadeSpeed;

        if((fadeSpeed < 0 && temp.a <= 0.0f) || (fadeSpeed > 0 && temp.a >= 1.0f))
        {
            temp.a = Mathf.Clamp(temp.a, 0.0f, 1.0f);
            fadeSpeed *= -1.0f;
        }
        text.color = temp;
	}
}
