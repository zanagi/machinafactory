* xyzrobot; right
The XYZ; Hey, Chief!
The XYZ; I'm sure you know this but making XYZs requires 3 Xs, Ys, Zs each.
The XYZ; When there are not enough items you should put me to sleep so that I won't waste my energy.
The XYZ; Oh! But don't forget to wake me up! I'm THE only XYZ robot in factory after all.
* hide; right
You can put a robot to sleep by clicking the sleep icon on its window.
Robots that are asleep use less energy but don't work. Wake them up when they're needed and put them to sleep when not.