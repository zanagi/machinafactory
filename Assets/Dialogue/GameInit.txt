Many years later...
* rowbat_cry; right
Rowbat; Chief Meccha! Chief Meccha!
* meccha; left
Meccha; What is it, assistant Rowbat?
Rowbat; The government sent a terrible message!
Rowbat; They said that Machina Factory is no longer of any value to them and that they'll cut down the power supply...
Meccha; WHAT?! But that means that this factory is running solely on its backup power plant. And it will eventually run out of power...
Meccha; Which means... that we robots won't be ablo recharge our robot selves... and be forced to shut down...
Rowbat; What should we do, Chief?!
Meccha; ...
Meccha; Professor ordered me to make this factory the best in the world. I won't let it go down like this! Even if it costs me my battery life!
Meccha; We will use the remaining power to make as many XYZs as possible and prove our value to the government!
Meccha; Assistan Rowbat! Are you with me?
* rowbat; right
Rowbat; Till shutdown do us part, Chief!
* hide; all
The end of Machina Factory is coming. You must generate as much value as possible before the power runs out.
Making Xs, Ys and Zs generates some value. Making XYZs generates a lot of value!
Robots (that are not sleeping) work on machines that make these items. Click on robots to affect their behaviour.
Robots can also "transfer" all of their remaining power to the factory. When a robot runs out of power it will stop working.