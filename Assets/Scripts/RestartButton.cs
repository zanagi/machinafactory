﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartButton : MonoBehaviour {

	public void Restart()
    {
        if(GameManager.Instance.Idle)
            LoadingManager.Instance.LoadScene(Scenes.Restart);
    }
}
