﻿using UnityEngine;
using System.Collections;

public class ToMenuEvent : MiniEvent
{
    protected override void CompleteEvent()
    {
        LoadingManager.Instance.LoadScene(Scenes.Menu);
        base.CompleteEvent();
    }

    protected override void LateUpdate()
    {
        if (CanUpdate)
            CompleteEvent();
    }
}
