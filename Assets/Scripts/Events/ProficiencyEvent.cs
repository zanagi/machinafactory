﻿using UnityEngine;
using System.Collections;

public class ProficiencyEvent : MiniEvent
{
    public int proficiencyDelta = 2;

    protected override void CompleteEvent()
    {
        var robot = GetComponentInParent<Robot>();
        robot.proficiency += proficiencyDelta;

        if (nextEvent)
        {
            nextEvent.active = true;
        }
        else
        {
            GameManager.Instance.SetState(GameState.Idle);
        }
        base.CompleteEvent();
    }

    protected override void LateUpdate()
    {
        if (CanUpdate)
            CompleteEvent();
    }
}
