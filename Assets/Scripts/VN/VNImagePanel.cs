﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ImagePanelPosition
{
    Left, Right, All
}

public class VNImagePanel : MonoBehaviour
{
    public bool Complete { get; private set; } // Is last action complete?

    [SerializeField]
    private VNImage left;
    [SerializeField]
    private VNImage right;

    private VNImage target;
    private static readonly string hideKeyword = "hide";

    public void SetImage(string name, ImagePanelPosition position)
    {
        if (position == ImagePanelPosition.All)
        {
            SetImage(name, ImagePanelPosition.Left);
            SetImage(name, ImagePanelPosition.Right);
            return;
        }
        Complete = false;
        target = (position == ImagePanelPosition.Left) ? left : right;
        
        if (name == hideKeyword)
        {
            if(!target.gameObject.activeSelf)
            {
                Complete = true;
                return;
            }
            target.Hide();
            return;
        }
        Sprite sprite = ContentManager.Instance.GetSprite(name);

        if (sprite)
        {
            if (target.gameObject.activeSelf)
            {
                target.Image.sprite = sprite;
                Complete = true;
                return;
            }
            target.Show(sprite);
            return;
        }
        Complete = true;
    }
    
    public void OnShowComplete()
    {
        Complete = true;
    }

    public void OnHideComplete()
    {
        Complete = true;
    }
}
