﻿using UnityEngine;
using System.Collections;

public class Scenes
{
    public static readonly string Menu = "Menu";
    public static readonly string Intro = "Intro";
    public static readonly string Game = "Game";
    public static readonly string Restart = "Restart";
    public static readonly string GoodEnding = "GoodEnding";
    public static readonly string BadEnding = "BadEnding";
}
