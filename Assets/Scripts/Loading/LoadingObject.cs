﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingObject : MonoBehaviour {

    private static LoadingObject instance;

    // Use this for initialization
    void Start () {
        if (instance)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
	}
}
