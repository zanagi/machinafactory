﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
    public static LoadingManager Instance { get; private set; }

    private AsyncOperation loadOperation;
    private string targetSceneName;

    private void Start()
    {
        Instance = this;
    }

    private void Update()
    {
        if (loadOperation == null)
            return;

        if (loadOperation.isDone)
        {
            loadOperation = null;
            LoadingScreen.Instance.StartOutAnimation();
            LoadingScreen.Instance.CompleteHandler += EndLoadingState;
        }
    }

    public void LoadScene(string targetSceneName, LoadingStyle loadingStyle = LoadingStyle.Fade)
    {
        if(targetSceneName == Scenes.Menu)
        {
            Restart.Skip = true;
        }
        SceneManager.LoadSceneAsync(targetSceneName);

        // Some major bug with loading
        /*
        Debug.Log("Load scene: " + targetSceneName);
        this.targetSceneName = targetSceneName;
        LoadingScreen.Instance.StartInAnimation(loadingStyle);
        LoadingScreen.Instance.CompleteHandler += BeginLoadingScene;

        // Set loading state
        if (GameManager.Instance)
            GameManager.Instance.SetState(GameState.Loading);
            */
    }

    private void BeginLoadingScene()
    {
        loadOperation = SceneManager.LoadSceneAsync(targetSceneName);
        LoadingScreen.Instance.CompleteHandler -= BeginLoadingScene;
    }

    private void EndLoadingState()
    {
        LoadingScreen.Instance.Hide();
        LoadingScreen.Instance.CompleteHandler -= EndLoadingState;

        // Set idle state
        if (GameManager.Instance)
            GameManager.Instance.SetState(GameState.Idle);
    }

    public string ActiveSceneName
    {
        get { return SceneManager.GetActiveScene().name; }
    }
} 
