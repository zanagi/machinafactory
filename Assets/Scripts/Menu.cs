﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

    private bool check;
	// Update is called once per frame
	
    public void StartGame()
    {
        LoadingManager.Instance.LoadScene(Scenes.Intro);
    }

    public void SkipToGame()
    {
        LoadingManager.Instance.LoadScene(Scenes.Restart);
    }
}
