﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Restart : MonoBehaviour {
    public static bool Skip;

    private bool complete;

    private void LateUpdate()
    {
        if (complete || LoadingScreen.Instance.IsLoading)
            return;
        StartCoroutine(ToGame());
    }

    private IEnumerator ToGame(float time = 1.0f)
    {
        complete = true;
        yield return new WaitForSeconds(time);
        Skip = true;
        LoadingManager.Instance.LoadScene(Scenes.Game);
    }
}
