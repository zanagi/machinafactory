﻿using UnityEngine;
using System.Collections;

public class RobotWorkCheck : RobotEventChecker
{
    public float requiredWork;
    public int proficiencyDelta = -2;
    private bool pCheck;

    private void LateUpdate()
    {
        if (!GameManager.Instance.Idle || robot.workDone < requiredWork)
            return;

        if (complete)
        {
            Destroy(gameObject);
            return;
        }

        if(!pCheck)
        {
            pCheck = true;
            robot.proficiency += proficiencyDelta;
        }

        complete = robot.SetNextEvent(eventPrefab);
    }
}
