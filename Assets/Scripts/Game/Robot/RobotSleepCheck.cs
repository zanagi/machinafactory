﻿using UnityEngine;
using System.Collections;

public class RobotSleepCheck : RobotEventChecker
{
    private void LateUpdate()
    {
        if (!GameManager.Instance.Idle)
            return;

        robot.SwapSleep();
        complete = true;
        Destroy(gameObject);
    }
}
