﻿using UnityEngine;
using System.Collections;

public class XYZMachine : Machine
{
    private static readonly int requiredResourceCount = 3;
    private bool resourcesTaken;

    public override float AddProgress(float amount)
    {
        if (amount <= 0)
            return 0;

        if(!resourcesTaken)
        {
            if(!GameManager.Instance.resourceContainer.HasBasicResources(requiredResourceCount))
            {
                return 0;
            }
            GameManager.Instance.resourceContainer.TakeBasicResources(requiredResourceCount);
            resourcesTaken = true;
        }
        resourceProgress += amount;

        if (resourceProgress >= requiredResourceProgress)
        {
            resourceProgress = 0; // Not a basic resource, so set progress to zero after completion
            GameManager.Instance.resourceContainer.AddResource(resource, slideArea);
            resourcesTaken = false;
        }
        return amount;
    }
}
