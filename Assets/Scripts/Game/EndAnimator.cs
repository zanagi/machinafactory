﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAnimator : MonoBehaviour {

    [SerializeField]
    private int goodEndingValue = 7;

	public void GoToEndScene()
    {
        if (GameManager.Instance.cycleCounter.cycleCount >= goodEndingValue)
        {
            LoadingManager.Instance.LoadScene(Scenes.GoodEnding);
            return;
        }
        LoadingManager.Instance.LoadScene(Scenes.BadEnding);
    }
}
